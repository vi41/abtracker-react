import {Component}  from 'react';
import authRequired from 'app/auth/auth-required';
import {connect} from 'react-redux';

@connect()
class Chat extends Component {

  render() {
    return <div>
      <h1>There will be chat</h1>;
    </div>
  }
}

export default authRequired(Chat);
