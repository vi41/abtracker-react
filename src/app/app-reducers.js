'use strict';

import {combineReducers} from 'redux';
import {authReducer} from './auth/auth-reducer';
import {userReducer} from './user/user-reducer';

export default combineReducers({auth: authReducer, user: userReducer});