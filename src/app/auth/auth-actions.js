'use strict';

import {server} from 'services/server';

export const ACTION_TYPE = {
  login_state_changed: 'AUTH/LOGIN_STATE_CHANGED',
  authentication_in_progress: 'AUTH/AUTHENTICATION_IN_PROGRESS',
  clear_error: 'AUTH/CLEAR_ERROR'
};

export function login(userName, password) {
  return dispatch => {
    dispatch(setProgress(true));
    server.login(userName, password);
  };
}

export function register(userName, firstName, lastName, password) {
  return dispatch => {
    dispatch(setProgress(true));
    server.makeRequest('/rest/register', {
      name: userName,
      firstName: firstName,
      lastName: lastName,
      password: password
    }).then(() => dispatch(login(userName, password)));
  };
}

export function setProgress(inProgress) {
  return {
    type: ACTION_TYPE.authentication_in_progress,
    payload: {
      inProgress: inProgress
    }
  }
}

export function clearError() {
  return {
    type: ACTION_TYPE.clear_error,
  }
}

export function loginStateChanged() {
  return {
    type: ACTION_TYPE.login_state_changed,
  }
}

export function logout() {
  return () => {
    server.logout();
  };
}

export default {login, register, loginStateChanged, setProgress, clearError, logout}