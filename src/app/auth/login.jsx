'use strict';

import './auth.less';
import {Component}  from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import actions from './auth-actions';
import {browserHistory} from 'react-router';
import GlobalProgress from 'app/progress.jsx'


@connect(store => {
  return {
    isLoggedIn: store.app.auth.isLoggedIn,
    loginError: store.app.auth.loginError,
    inProgress: store.app.auth.inProgress,
    user: store.app.user
  }
}, dispatch => {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
})
export default class Login extends Component {
  constructor() {
    super();
    this._userName = 'admin';
    this._password = 'qwerty';
  }

  _setUserName(evt) {
    this._userName = evt.target.value;
  }

  _setPassword(evt) {
    this._password = evt.target.value;
  }

  _login() {
    this.props.actions.login(this._userName, this._password);
  }

  _signUp() {
    this.props.actions.clearError();
    browserHistory.push('/register');
  }

  componentWillReceiveProps(nextProps) {
    nextProps.isLoggedIn && !this.props.isLoggedIn && browserHistory.push('/chat');
  }

  render() {
    let {loginError, inProgress} = this.props;
    return <div className='auth fill-page content-center'>
      <GlobalProgress enable={inProgress}/>
      <form>
        <h4>Login to your account</h4>
        <input type='text' className='form-control' placeholder='Username' defaultValue={this._userName}
               onChange={::this._setUserName}/>
        <input type='password' className='form-control' placeholder='Username' defaultValue={this._password}
               onChange={::this._setPassword}/>
        <button type='button' onClick={::this._login} className='btn btn-info'>Login</button>
        <button type='button' onClick={::this._signUp} className='btn btn-info'>Sign Up</button>
        <span className='text-danger text-center'>{loginError}</span>
      </form>
    </div>
  }
}



