'use strict';

import React  from 'react';
import {connect} from 'react-redux';
import {browserHistory} from 'react-router';

@connect(store => {
  return {
    isLoggedIn: store.app.auth.isLoggedIn
  }
})
export default function (Component) {
  return class AuthRequired extends React.Component {

    componentDidMount() {
      this._checkAuthenticated(this.props);
    }

    componentWillReceiveProps(nextProps) {
      this._checkAuthenticated(nextProps);
    }

    _checkAuthenticated(props) {
      !props.isLoggedIn && browserHistory.push('/login');
    }

    render() {
      return this.props.isLoggedIn && <Component {...this.props} />
    }
  }
}