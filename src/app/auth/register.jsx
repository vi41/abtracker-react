'use strict';

import './auth.less';
import {Component}  from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import actions from './auth-actions';
import {browserHistory} from 'react-router';
import GlobalProgress from 'app/progress.jsx';

@connect(store => {
  return {
    isLoggedIn: store.app.auth.isLoggedIn,
    loginError: store.app.auth.loginError,
    inProgress: store.app.auth.inProgress
  }
}, dispatch => {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
})
export default class Register extends Component {
  constructor() {
    super();
    this._userName = '';
    this._firstName = '';
    this._lastName = '';
    this._password = '';
  }

  _setUserName(evt) {
    this._userName = evt.target.value;
  }

  _setFirstName(evt) {
    this._firstName = evt.target.value;
  }

  _setLastName(evt) {
    this._lastName = evt.target.value;
  }

  _setPassword(evt) {
    this._password = evt.target.value;
  }

  _signUp() {
    this.props.actions.register(this._userName, this._firstName, this._lastName, this._password);
  }

  componentWillReceiveProps(nextProps) {
    nextProps.isLoggedIn && !this.props.isLoggedIn && browserHistory.push('/chat');
  }

  render() {
    let {loginError, inProgress} = this.props;
    return <div className='auth content-center'>
      <GlobalProgress enable={inProgress}/>
      <form>
        <h4 className='header'>Create new client account</h4>
        <input type='text' className='form-control' onChange={::this._setUserName} placeholder='Username'/>
        <input type='text' className='form-control' onChange={::this._setFirstName} placeholder='First name'/>
        <input type='text' className='form-control' onChange={::this._setLastName} placeholder='Last name'/>
        <input type='password' className='form-control' onChange={::this._setPassword} placeholder='Password'/>
        <button type='button' onClick={::this._signUp} className='btn btn-info'>Sign Up</button>
        <span className='text-danger text-center'>{loginError}</span>
      </form>
    </div>;
  }
}



