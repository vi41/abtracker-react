'use strict';

import {server, SERVER_EVENTS} from 'services/server';
import {ACTION_TYPE} from './auth-actions';

const initialState = {
  isLoggedIn: false,
  loginError: '',
  inProgress: false,
  user: null
};

function getErrorDescription(action) {
  let error = action.payload.error || '';
  return error ? error.description : '';
}

export function authReducer(state = initialState, action) {
  let res = {...state};
  res.error = '';
  switch (action.type) {
    case SERVER_EVENTS.login_error:
      res.inProgress = false;
      res.loginError = getErrorDescription(action);
      break;
    case ACTION_TYPE.clear_error:
      res.loginError = '';
      break;
    case ACTION_TYPE.authentication_in_progress:
      res.inProgress = action.payload.inProgress;
      break;
    case SERVER_EVENTS.session_valid:
      res.inProgress = false;
      break;
  }
  res.isLoggedIn = !res.inProgress && server.isLoggedIn();
  return res;
}