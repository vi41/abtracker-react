'use strict';

import './auth.less';
import {Component} from 'react';
import {Link} from 'react-router';
import actions from './auth-actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

@connect(null, dispatch => {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
})
export default class RegisterHeader extends Component {
  render() {
    return <div className='register'>
      <Link to='/login' onClick={::this.props.actions.clearError}><span className='glyphicon glyphicon-chevron-left'></span>Back to login</Link>
    </div>
  }
}