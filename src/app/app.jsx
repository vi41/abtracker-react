'use strict';

import './app.less';
import {Component} from 'react';
import actions from './auth/auth-actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Header from './header';

@connect(null, dispatch => {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
})
export default class App extends Component {

  render() {
    let {router} = this.props;
    return <div className='app fill-page'>
      <Header router={router}/>
      {this.props.children}
    </div>
  }
}