'use strict';

import {Route, IndexRedirect} from 'react-router';
import Login from './auth/login';
import Register from './auth/register'
import App from './app';
import Chat from 'chat/chat';

export default (
  <div>
    <Route path='/' component={App}>
      <IndexRedirect to='chat'/>
      <Route path='login' component={Login}/>
      <Route path='register' component={Register}/>
      <Route path='chat' component={Chat}/>
    </Route>
    <Route path='*'>
      <IndexRedirect to='/chat'/>
    </Route>
  </div>
);