'use strict';

import './app.less';
import {Component} from 'react';
import {Navbar, Nav, NavDropdown, MenuItem} from 'react-bootstrap';
import authActions from './auth/auth-actions';
import userActions from './user/user-actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

@connect(store => {
  return {
    user: store.app.user
  }
}, dispatch => {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
  }
})
export default class DashboardHeader extends Component {
  _getUserTitle() {
    let {user} = this.props;
    user = user || {};
    let title = '';
    if (user.firstName) {
      title += user.firstName;
    }
    if (user.lastName) {
      if (title) {
        title += ' ';
      }
      title += user.lastName;
    }
    title = !title ? user.name : title;
    return title || '';
  }

  render() {
    let {status} = this.props.user;
    status = status ? status.toLowerCase() : 'connecting';
    return <Navbar className='dashboard-header'>
      <Navbar.Brand>
        <a>AB Tracker</a>
      </Navbar.Brand>
      <Nav className='main-menu' pullRight>
        <NavDropdown title={this._getUserTitle()} className={`menu-icon ${status}`} id='basic-nav-dropdown' pullRight>
          <MenuItem className='menu-icon online' onClick={this.props.userActions.setOnlineStatus}>Online</MenuItem>
          <MenuItem className='menu-icon away' onClick={this.props.userActions.setAwayStatus}>Away</MenuItem>
          <MenuItem className='menu-icon offline' onClick={this.props.userActions.setOfflineStatus}>Offline</MenuItem>
          <MenuItem className='menu-icon logout' onClick={this.props.authActions.logout}>Logout</MenuItem>
        </NavDropdown>
      </Nav>
    </Navbar>
  }
}
