'use strict';

import './app.less'
import {Component} from 'react';

export default class GlobalProgress extends Component {
  render() {
    let {enable} = this.props;
    let componentClass = `global-progress fill-page ${!enable ? 'invisible' : ''}`;
    return  <div className={componentClass}></div>;
  }
}