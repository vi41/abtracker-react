'use strict';

import {server, SERVER_EVENTS} from 'services/server';
import store from 'store';

const USER_KEY = 'ABTracker.user';

let user;
if (server.isLoggedIn()) {
  user = store.get(USER_KEY);
  if(user){
    user.status = server.getStatus();
  }
}

const initialState = user || {id: null, name: null, first_name: null, last_name: null, status: null};

export function userReducer(state = initialState, action) {
  let res = state;
  switch (action.type) {
    case SERVER_EVENTS.session_valid:
      res = {...state, ...action.payload, status: null};
      store.set(USER_KEY, res);
      res.status = server.getStatus();
      break;
    case SERVER_EVENTS.status_changed:
      res = {...state, status: server.getStatus()};
      break;
  }
  return res;
}