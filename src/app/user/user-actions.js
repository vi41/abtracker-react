'use strict';

import {server, USER_STATUS} from 'services/server';

function setOnlineStatus() {
  return setStatus(USER_STATUS.online);
}

function setAwayStatus() {
  return setStatus(USER_STATUS.away);
}

function setOfflineStatus() {
  return setStatus(USER_STATUS.offline);
}

function setStatus(status) {
  return () => server.setStatus(status);
}

export default {setOnlineStatus, setAwayStatus, setOfflineStatus};