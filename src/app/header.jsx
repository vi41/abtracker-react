'use strict';

import {Component} from 'react';
import RegisterHeader from './auth/register-header';
import DashboardHeader from './dashboard-header';

export default class Header extends Component {
  render() {
    let {router} = this.props;
    let location = this.props.router.getCurrentLocation();
    let currHeader = null;
    switch (location.pathname) {
      case '/register':
        currHeader = <RegisterHeader router={router}/>;
        break;
      case '/chat':
        currHeader = <DashboardHeader/>;
        break;
    }

    return <div className='page-header'>
      {currHeader}
    </div>
  }
}