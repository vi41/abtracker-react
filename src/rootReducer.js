'use strict';

import app from 'app/app-reducers';
import {combineReducers} from 'redux';

export default combineReducers({app});