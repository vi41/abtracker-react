'use strict';

import {Promise} from 'es6-promise';

export function dispatch(event) {
  let store = require('./app-store');
  store.dispatch(event);
}

export function buildAction(type, payload) {
  return {
    type: type,
    payload: payload
  }
}

export function async(proc, ...params) {
  var iterator = proc(...params);
  return new Promise((resolve, reject) => {
    let loop = (value) => {
      let result;
      try {
        result = iterator.next(value)
      }
      catch (err) {
        reject(err)
      }
      if (result.done)
        resolve(result.value);
      else if (typeof result.value === 'object'
        && typeof result.value.then === 'function')
        result.value.then((value) => {
          loop(value)
        }, (err) => {
          reject(err)
        });
      else
        loop(result.value)
    };
    loop()
  })
}