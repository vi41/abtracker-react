'use strict';

import SockJS from 'sockjs-client';
import webstomp from 'webstomp-client';
import EventEmitter from 'event-emitter';

export const SOCKET_STATUS = {
  connected: 'CONNECTED',
  connecting: 'CONNECTING',
  disconnected: 'DISCONNECTED',
  disconnecting: 'DISCONNECTING'
};

const RECONNECT_SOCKET_INTERVAL_MS = 15 * 1000;

class WebSocketService {

  static EVENTS = {
    inbound_message: 'inbound_message',
    status_changed: 'status_changed'
  };

  constructor() {
    this._initialState();
    this._webSocket = null;
    this._stompClient = null;
    this._reconnectTaskId = null;
    this._status = SOCKET_STATUS.disconnected;
    this._eventEmmiter = new EventEmitter();
  }

  _initialState() {
    this._url = null;
    this._accessTokenProvider = null;
  }

  _clearWebSocket() {
    this._stompClient = null;
    this._webSocket = null;
  }

  connect(url, accessTokenProvider) {
    if (!this._stompClient) {
      this._url = url;
      this._accessTokenProvider = accessTokenProvider;
      this._startConnection();
    }
  }

  getStatus() {
    return this._status;
  }

  disconnect() {
    if (this._status !== SOCKET_STATUS.disconnected) {
      this._initialState();
      clearTimeout(this._reconnectTaskId);
      this._reconnectTaskId = null;
      this._setStatus(SOCKET_STATUS.disconnecting);
      if (this._stompClient) {
        this._stompClient.disconnect(() => {
          this._clearWebSocket();
          this._setStatus(SOCKET_STATUS.disconnected);
        });
      } else {
        this._setStatus(SOCKET_STATUS.disconnected);
      }
    }
  }


  _startConnection() {
    let accessToken = this._accessTokenProvider.getAccessToken();
    if (!accessToken) {
      this._initialState();
      return;
    }
    this._setStatus(SOCKET_STATUS.connecting);
    this._webSocket = new SockJS(`${this._url}/socket?accessToken=${accessToken}`);
    this._stompClient = webstomp.over(this._webSocket);
    this._stompClient.hasDebug = false;
    this._stompClient.connect({}, () => {
      this._setStatus(SOCKET_STATUS.connected);
      this._stompClient.subscribe('/user/queue/main',
        (message) => message.body && this._eventEmmiter.emit(WebSocketService.EVENTS.inbound_message,
          JSON.parse(message.body)));
    }, (event) => {
      if (event.type === 'close') {
        this._clearWebSocket();
        this._plainReconnect();
      }
    });
  }

  _plainReconnect() {
    if (!this._reconnectTaskId && this._url && this._accessTokenProvider
      && this._status !== SOCKET_STATUS.disconnecting) {
      this._setStatus(SOCKET_STATUS.connecting);
      this._reconnectTaskId = setTimeout(() => {
        this._startConnection();
        this._reconnectTaskId = null;
      }, RECONNECT_SOCKET_INTERVAL_MS);
    }
  }

  _setStatus(status) {
    if (status !== this._status) {
      let event = {oldStatus: this._status, newStatus: status};
      this._status = status;
      this._eventEmmiter.emit(WebSocketService.EVENTS.status_changed, event);
    }
  }

  sendMessage(destination, body) {
    if (this._stompClient) {
      this._stompClient.send(destination, body ? JSON.stringify(body) : undefined);
    }
  }

  _on(event, listener) {
    this._eventEmmiter.on(event, listener);
    return this._off.bind(this, event, listener);
  }

  _off(event, listener) {
    this._eventEmmiter.off(event, listener);
  }

  addInboundMessageListener(listener) {
    return this._on(WebSocketService.EVENTS.inbound_message, listener);
  }

  addStatusChangeListener(listener) {
    return this._on(WebSocketService.EVENTS.status_changed, listener);
  }
}

export const webSocket =  new WebSocketService();