'use strict';

import ajax from '@fdaciuk/ajax';
import {Promise} from 'es6-promise';
import EventEmitter from 'event-emitter';
import * as _ from 'lodash';
import store from 'store';

export class ServerError extends Error {

  constructor(response, status) {
    let description = ServerError._getErrorTitle(response, status);
    super(description ? `${status} (${description})` : status, status);
    Error.captureStackTrace(this, ServerError);
    this._status = status;
    this.description = description ? description : `Error: ${status}`;
  }

  static _getErrorTitle(response, status) {
    let title = '';
    switch (status) {
      case 400:
        title = response.error === 'invalid_grant' ? 'Session expired' : '';
        break;
      case 401:
        title = 'Session expired';
        break;
      case 500:
        title = 'Unable to connect to server';
        break;
      case 404:
        title = 'Cannot execute server request'
    }
    return title || response.error_description || response.error || response;
  }
}

const SESSION_KEY = 'ABTracker.session', REFRESH_TOKEN_INTERVAL_IN_SEC = 5;

class RestService {

  static EVENTS = {
    login_error: 'login_error',
    login_state_changed: 'login_state_changed',
    access_token_refreshed: 'access_token_refreshed'
  };

  constructor() {
    this._session = null;
    this._refreshTokenTaskInProgress = false;
    this._refreshTokenSchedulerTaskId = null;
    this._suspendedRequests = [];
    this._loadSession();
    this._eventEmmiter = new EventEmitter();
    this._config = null;
  }

  getServerUrl() {
    return this._loadConfig().then(config => config.app_server_url);
  }

  getAuthServerUrl() {
    return this._loadConfig().then(config => config.auth_server_url);
  }

  _getAuthenticationOptions(data, headers) {
    return this.getAuthServerUrl().then((serverURL) => {
      headers = headers || {};
      headers['Content-Type'] = 'application/x-www-form-urlencoded';
      return {
        url: `${serverURL}/oauth/token`,
        method: 'POST',
        headers: headers,
        data: data
      };
    })
  }

  _isInvalidTokenError(error) {
    return error._status === 401;
  }

  _loadSession() {
    this._session = store.get(SESSION_KEY);
  }

  _loadConfig() {
    return new Promise((resolve, reject) => {
      if (!this._config) {
        this._makeAjaxRequest(this._buildRequestOptions('/config')).then((config) => {
          this._config = config;
          resolve(config)
        }).catch(reject);
      } else {
        resolve(this._config);
      }
    });
  }


  _getSession() {
    return this._session || {};
  }

  getAccessToken() {
    return this._getSession()['access_token'];
  }

  _getRefreshToken() {
    return this._getSession()['refresh_token'];
  }

  _getAuthorizationHeaderValue() {
    let token = this.getAccessToken();
    return token ? `Bearer ${token}` : null;
  }

  _setAuthorizationHeader(options) {
    let header = this._getAuthorizationHeaderValue();
    if (header) {
      options.headers = options.headers || [];
      options.headers['Authorization'] = header;
    }
    return options;
  }

  _continueRequests() {
    let suspendedRequests = this._suspendedRequests;
    this._suspendedRequests = [];
    _.forEach(suspendedRequests, (request) => {
      this._setAuthorizationHeader(request.options);
      this._makeAjaxRequest(request.options).then(response => request.resolve(response))
        .catch((error) => {
          this._setSessionData(null, error);
          request.reject(error);
        });
    });
  }

  _stopRequests(error) {
    let suspendedRequests = this._suspendedRequests;
    this._suspendedRequests = [];
    _.forEach(suspendedRequests, request => request.reject(error));
  }

  _makeAjaxRequest(options) {
    return new Promise((resolve, reject) => {
      ajax(options).always((response, request) => {
        request.status !== 200 ? reject(new ServerError(response, request.status)) : resolve(response);
      });
    });
  }

  _suspendRequest(options, resolve, reject) {
    if (this._session) {
      this._suspendedRequests.push({
        options: options,
        resolve: resolve,
        reject: reject
      });
    }
  }

  refreshAccessToken() {
    if (!this._refreshTokenTaskInProgress && this._session) {
      this._refreshTokenTaskInProgress = true;
      return this._getAuthenticationOptions(`grant_type=refresh_token&refresh_token=${this._getRefreshToken()}`)
        .then((options) => this._makeAjaxRequest(options)).then(response => {
          if (this._session && this._session.refresh_token === response.refresh_token) {
            this._refreshTokenTaskInProgress = false;
            this._setSessionData(response);
            this._continueRequests();
          }
        }).catch(error => {
          this._refreshTokenTaskInProgress = false;
          this._stopRequests(error);
          this._setSessionData(null, error);
        });
    }
  }

  _buildRequestOptions(url, data, method) {
    let headers = {};
    headers['Content-Type'] = 'application/json';
    let options = {
      url: url,
      method: method || 'POST',
      headers: headers
    };
    if (data) {
      options['data'] = JSON.stringify(data);
    }
    this._setAuthorizationHeader(options);
    return options;
  }


  makeRequest(url, data) {
    let options = this._buildRequestOptions(url, data);
    return new Promise((resolve, reject) => {
      this.getServerUrl().then(serverURL => {
        url = `${serverURL}${url}`;
        options = this._buildRequestOptions(url, data);
        return this._makeAjaxRequest(options)
      }).then(resolve).catch(error => {
        if (this._isInvalidTokenError(error) && this.isLoggedIn()) {
          this._suspendRequest(options, resolve, reject);
          this.refreshAccessToken()
        } else {
          reject(error);
        }
      })
    });
  }

  login(username, password) {
    this._setSessionData(null);
    return this._getAuthenticationOptions(`grant_type=password&username=${username}&password=${password}`)
      .then(options => this._makeAjaxRequest(options)).then((response) => {
        this._setSessionData(response);
        return response;
      });
  }

  isLoggedIn() {
    return !!this._session;
  }

  logout() {
    this._setSessionData(null);
  }

  _scheduleRefreshAccessToken() {
    clearTimeout(this._refreshTokenSchedulerTaskId);
    if (this._session && this._session.expires_in > 0) {
      let delay = this._session.expires_in - REFRESH_TOKEN_INTERVAL_IN_SEC;
      delay = delay >= 0 ? delay : 0;
      this._refreshTokenSchedulerTaskId = setTimeout(::this.refreshAccessToken, delay * 1000);
    }
  }

  _setSessionData(data, error) {
    let isLoggedIn = this.isLoggedIn();
    let options = this._session && !data && !error ? this._buildRequestOptions('/rest/logout') : null;
    data !== null ? store.set(SESSION_KEY, data) : store.remove(SESSION_KEY);
    this._session = data;
    if (options) {
      this._makeAjaxRequest(options);
      this._suspendedRequests = [];
    }
    if (isLoggedIn !== this.isLoggedIn()) {
      error ? this._eventEmmiter.emit(RestService.EVENTS.login_error, error)
        : this._eventEmmiter.emit(RestService.EVENTS.login_state_changed,
        {oldState: isLoggedIn, newState: this.isLoggedIn()});
    }
    if (this.isLoggedIn()) {
      this._eventEmmiter.emit(RestService.EVENTS.access_token_refreshed, data);
    }
    this._scheduleRefreshAccessToken();
  }

  _on(event, listener) {
    this._eventEmmiter.on(event, listener);
    return this._off.bind(this, event, listener);
  }

  _off(event, listener) {
    this._eventEmmiter.off(event, listener);
  }

  addLoginErrorsListener(listener) {
    return this._on(RestService.EVENTS.login_error, listener);
  }

  addLoginStateChanged(listener) {
    return this._on(RestService.EVENTS.login_state_changed, listener);
  }

  addRefreshAccessTokenListener(listener) {
    return this._on(RestService.EVENTS.access_token_refreshed, listener);
  }
}

export default new RestService();