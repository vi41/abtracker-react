'use strict';

import restService from './rest-service';
import {webSocket, SOCKET_STATUS} from './web-socket';
import {dispatch} from './../utils';
import {loginStateChanged} from 'app/auth/auth-actions';
import {async} from './../utils';
import store from 'store';

const STATUS_KEY = 'ABTracker.user_status';

export const USER_STATUS = {
  online: 'ONLINE',
  away: 'AWAY',
  offline: 'OFFLINE'
};

export const SERVER_EVENTS = {
  session_valid: 'SERVER/SESSION_VALID',
  login_error: 'SERVER/LOGIN_ERROR',
  inbound_message: 'SERVER/INBOUND_MESSAGE',
  status_changed: 'SERVER/STATUS'
};

function inboundMessage(message) {
  return {
    type: SERVER_EVENTS.inbound_message,
    payload: message
  };
}

function sessionValid(user) {
  return {
    type: SERVER_EVENTS.session_valid,
    payload: user
  }
}

function loginError(error) {
  return {
    type: SERVER_EVENTS.login_error,
    payload: {
      error: error
    }
  }
}

function statusChanged(oldStatus, newStatus) {
  return {
    type: SERVER_EVENTS.status_changed,
    payload: {
      oldStatus: oldStatus,
      newStatus: newStatus
    }
  }
}

class ServerAPIService {

  constructor() {
    this._initListeners();
    this._restoreSession();
    this._lastStatus = store.get(STATUS_KEY) || USER_STATUS.online;
    this._status = null;
  }

  _initListeners() {
    restService.addLoginErrorsListener((error) => dispatch(loginError(error)));
    restService.addLoginStateChanged((data) => {
      !data.newState && webSocket.disconnect();
      dispatch(loginStateChanged())
    });
    restService.addRefreshAccessTokenListener(() => webSocket.getStatus() === SOCKET_STATUS.disconnected &&
    restService.getServerUrl().then(url => webSocket.connect(url, restService)));
    webSocket.addInboundMessageListener((message) => {
      message = inboundMessage(message);
      dispatch(message);
      message.payload.message_type === 'user_status' && this._statusConfirmed(message.payload.status);
    });
    webSocket.addStatusChangeListener((event) => {
      if (event.newStatus === SOCKET_STATUS.connected) {
        this._sendLastStatus();
      } else {
        this._statusConfirmed(null);
      }
    });
  }

  _restoreSession() {
    let that = this;
    if (restService.isLoggedIn()) {
      async(function*() {
        yield restService.refreshAccessToken();
        let user = yield that.makeRequest('/rest/user');
        dispatch(sessionValid(user));
        return user;
      });
    }
  }

  login(userName, password) {
    let that = this;
    async(function*() {
      yield restService.login(userName, password);
      let user = yield that.makeRequest('/rest/user');
      dispatch(sessionValid(user));
      return user;
    }).catch(error => dispatch(loginError(error)));
  }

  logout() {
    return restService.logout();
  }

  isLoggedIn() {
    return restService.isLoggedIn();
  }

  makeRequest(url, data) {
    return restService.makeRequest(url, data);
  }

  sendMessage(destination, body) {
    webSocket.sendMessage(destination, body);
  }

  _statusConfirmed(status) {
    if (status !== this._status) {
      let event = statusChanged(this._status, status);
      this._status = status;
      dispatch(event);
    }
  }

  getStatus() {
    return this._status;
  }

  _sendLastStatus() {
    this.sendMessage('/app/status', {status: this._lastStatus});
  }

  setStatus(status) {
    this._lastStatus = status;
    store.set(STATUS_KEY, status);
    this._sendLastStatus();
  }
}

export const server = new ServerAPIService();
