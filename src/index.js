'use strict';

import {render} from 'react-dom';
import routes from 'app/routes';
import {Router, browserHistory} from 'react-router'
import './index.less';
import {Provider} from 'react-redux';
import store from './app-store'

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('root')
);