'use strict';

const webpack = require('webpack');

module.exports = {
  entry: {
    'index': ['webpack-hot-middleware/client', './index']
  },

  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loaders: ['react-hot', 'babel-loader']
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
};
