'use strict';

const webpack = require('webpack');

module.exports = {
  entry: {
    'index': ['./index']
  },

  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        plugins: ['transform-runtime'],
        loaders: ['babel-loader']
      }
    ],
  },

  plugins: [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {unused: true, dead_code: true}
    })
  ]
};
