'use strict';

var webpack = require('webpack');
var express = require('express');
var fs = require('fs');
var https = require('https');
var app = express();
var env = require('./env.js');
var url = require('url');
const path = require('path');
var proxy = require('express-http-proxy');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var AUTHORIZATION_HEADER = 'Basic ' + new Buffer(env.app_name + ':' + env.app_secret).toString('base64');

var config = require('./webpack.config');

if(process.env.NODE_ENV === 'development'){
  var compiler = webpack(config);
  app.use(webpackDevMiddleware(compiler, {
    noInfo: true
  }));
  app.use(webpackHotMiddleware(compiler));

  app.get(/.*/, function root(req, res, next) {
    var filename = path.join(compiler.outputPath,'index.html');
    compiler.outputFileSystem.readFile(filename, function(err, result){
      if (err) {
        return next(err);
      }
      res.set('content-type','text/html');
      res.send(result);
      res.end();
    });
  });
} else {
  app.use(express.static(__dirname + '/build'));
  app.get(/.*/, function root(req, res) {
    res.sendFile(__dirname + '/build/index.html');
  });
}

/* Load https certificates */
var key = fs.readFileSync(env.priv_key, 'utf8');
var cert = fs.readFileSync(env.pub_key, 'utf8');

var https_options = {
  key: key,
  cert: cert
};

app.post('/config', function(req, res) {
  res.send({app_server_url: env.app_server_url, auth_server_url: env.auth_server_url});
});

/* Rest services proxy config */
app.use(['/rest', '/socket'], proxy(env.app_server_url, {
  forwardPath: function (req) {
    return '/rest' + url.parse(req.url).path;
  }
}));

app.use('/oauth', proxy(env.app_server_url, {
  forwardPath: function (req) {
    return '/oauth' + url.parse(req.url).path;
  },
  decorateRequest: function (proxyReq) {
    if (!proxyReq.headers['Authorization']) {
      proxyReq.headers['Authorization'] = AUTHORIZATION_HEADER;
    }
    return proxyReq;
  }
}));

/* Start https server */

https.createServer(https_options, app).listen(env.port, env.url);

console.info("Server is running on port: " + env.port);