'use strict';

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const merge = require('webpack-merge');

const production = require('./prod.config.js');
const development = require('./dev.config.js');

const ENV_TYPE = {
  dev: 'development',
  prod: 'production'
};

const TARGET = process.env.npm_lifecycle_event;

const NODE_ENV = TARGET === 'start-dev' ? ENV_TYPE.dev : ENV_TYPE.prod;
process.env.NODE_ENV = NODE_ENV;

console.info(NODE_ENV + ' environment');

const commonConfig = {
  context: path.resolve(__dirname, "src"),
  entry: {
    'vendors': ['./vendors', 'babel-polyfill'],
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: '[name]-[hash].js',
    library: '[name]',
  },

  module: {
    loaders: [
      {
        test: /\.html$/,
        loader: 'html?-minimize'
      },
      {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract(['css', 'less'])
      },
      {
        test: /\.(jpg|svg|png)$/i,
        loader: 'url-loader?name=assets/img/[name]-[hash].[ext]'
      },
      {
        test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)/,
        loader: 'url-loader?limit=1&name=assets/fonts/[name]-[hash].[ext]'
      }
    ],

    preLoaders: [
      {
        test: /\.(js|jsx)$/,
        loaders: ['eslint-loader'],
      }
    ],

  },

  devtool: NODE_ENV === ENV_TYPE.dev ? 'eval' : null,

  resolve: {
    root: path.resolve(__dirname, "src"),
    extensions: ['', '.js','.jsx']
  },

  resolveLoader: {
    modulesDirectories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extensions: ['', '.js']
  },

  plugins: [
    new CleanWebpackPlugin(['build/*'], {
      verbose: true,
      dry: false
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      minChunks: 2
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
    }),
    new webpack.ProvidePlugin({
      "React": "react",
    }),
    new HtmlWebpackPlugin({
      template: 'index.html',
      favicon: 'assets/favicon.ico'
    }),
    new ExtractTextPlugin('[name]-[hash].css')
  ]
};


if (NODE_ENV === ENV_TYPE.dev) {
  module.exports = merge(development, commonConfig);
} else {
  module.exports = merge(production, commonConfig);
}
