exports.port = 443;
exports.url = "localhost";
exports.priv_key = "key.pem";
exports.pub_key =  "server.crt";
exports.app_server_url = 'https://localhost:8087';
exports.auth_server_url = 'https://localhost';
exports.app_name = 'abtracker';
exports.app_secret = 'qwerty';